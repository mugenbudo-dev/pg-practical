Перед тем как начинать содержательную работу следует научиться базовым вещам:
Изначальный постгрес появился как монолитное решение, после чего в него годами
добавлялись возможности расширения его функциональности через внешние модули:
расширения. Для решения большинства задач нет необходимости изменять ядро самого
постгреса, достаточно реализовать требуемую функциональность в отдельном модуле
и подключить ее к основному постгресу через соответсвующие интерфейсы (хуки)

Поэтому в качестве нулевой задачи мы займемся созданием расширений как таковых,
пока что без смысловой нагрузки. Это будет расширение различными способами
выводящее сообщение "Hello World!"

Рекомендуемые источники для старта:
- Лекция курса DBA1: [Установка и управление сервером](https://edu.postgrespro.ru/dba1-13/dba1_01_tools_install.html)
- Лекция курса DEV2: [Создание расширений](https://edu.postgrespro.ru/dev2/dev2_14_ext_extensions.html)

Элементы Т.З. выстроенны от простого к сложному, начать надо с реализации первого и
постепенно наращивать сложность.

Создать расширение hello_world реализующиее на языке C следующие функции:

1. `hello_world_warn` - Выводит на экран предупрежение (warning) с текстом "Hello
   World". (hint: как выводть warning посмотрите в готовых расширениях из
   директории `conritb` там наверняка кто-то выводит предупреждения)
   
   **ТП: Нужна подсказка о том, что часто нужно поглядывать на секцию include чтобы что-то заработало :)**

2. `hello_world_value` - Возвращает в качестве результата своей работы строку
   "Hello world!", таким образом этой функцией можно воспользоваться выполнив
   запрос `SELECT hellow_world_value();` (hint: TODO куда бы тут послать
   человека)'

3. `hello_world_row` - возвращает запись с текстовыми значениям "Hello" и
   "World!" в первом и втором поле. (Не уверен что это на самом деле надо на
   этом этапе, может лишнее)
   
   **ТП: Можно отправить в доку: [https://postgrespro.ru/docs/postgresql/15/xfunc-c#id-1.8.3.13.11](https://postgrespro.ru/docs/postgresql/15/xfunc-c#id-1.8.3.13.11)**

4. `hello_param` - Параметризированная версия функции `hello_world_value`, принимает в
   качестве параметра строку, и возвращает строку "Hello, ARG!" где вместо ARG
   подставленно значение параметра.

Приборы и материалы:

1. Поисковик: https://duckduckgo.com/?q=create+postgres+extention&ia=web
   туториолы в сети есть, надо найти подходящий

2. Теоретическая часть: https://postgrespro.ru/docs/postgresql/15/extend, там
   особо интересен должен быть раздел "Функции на языке Си". Но там как-то
   слишком много букв на мой вкус; https://edu.postgrespro.ru/dev2/dev2_14_ext_extensions.html - полезная лекция из учебного курса.
   
   **ТП: Предлагаю еще добавить немного ссылок (или чего-то) про формирование Makefile'ов, было бы полезно.**

3. Практическая часть: если у вас есть какой-то опыт работы с постгресом, и вы
   знаете какую-то функцию какого-то расширения, которая делает что-то похожее
   (например возвращает текстовую строку, или принимает текстовый параметр),
   то можно и **нужно** смотреть в код как она это делает.

4. > "...и **нужно** смотреть в код"

   postgres/src/tutorial/

   https://github.com/postgres/postgres/tree/master/src/tutorial

   Директория в исходных кодах, в которой есть в т.ч. реализация пользовательского типа данных complex в файле complex.c.
